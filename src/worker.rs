use futures::prelude::*;
use hyper::server::conn::Http;
use hyper::service::{Service};
use hyper::{Body, Request, Response};
use tokio::net::{TcpStream, ToSocketAddrs};

pub async fn handle_connection<Svc>(service: Svc, connection: TcpStream) -> Result<(), hyper::Error>
where
    Svc: Service<Request<Body>, Response = Response<Body>> + Send + 'static,
    Svc::Future: Send + 'static,
    Svc::Error: std::error::Error + Sync + Send + 'static,
{
    Http::new()
        .http1_keep_alive(true)
        .http1_only(true)
        .serve_connection(connection, service)
        .await
}

pub async fn serve<Svc, Fut>(mut handler_provider: impl FnMut() -> Fut, broker: impl ToSocketAddrs)
where
    Svc: Service<Request<Body>, Response = Response<Body>> + Send + 'static,
    Svc::Future: Send + 'static,
    Svc::Error: std::error::Error + Sync + Send + 'static,
    Fut: Future<Output = Svc>,
{
    loop {
        let service = handler_provider().await;
        if let Ok(connection) = TcpStream::connect(&broker).await {
            eprintln!("DSDASDS");
            if let Err(e) = handle_connection(service, connection).await {
                eprintln!("Failed with error: {:?}", e);
            }
        }
    }
}
