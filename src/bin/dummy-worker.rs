use std::convert::Infallible;

use pothead::worker::serve;

use futures::prelude::*;
use hyper::service::service_fn;
use hyper::{Body, Request, Response};

async fn handle(_: Request<Body>) -> Result<Response<Body>, Infallible> {
    Ok(Response::new("Hello, World!".into()))
}

#[tokio::main]
async fn main() {
    let make_handler = move || future::ready(service_fn(handle));

    let broker_addr: std::net::SocketAddr = ([0, 0, 0, 0], 8081u16).into();

    serve(make_handler, broker_addr).await
}
