use pothead::broker;

#[tokio::main]
async fn main() {
    use clap::{value_t, App, Arg};

    let matches = App::new("PotHead Request Broker")
        .version("0.1")
        .author("Ulrik Mikaelsson <ulrik.mikaelsson@gmail.com>")
        .about("Matches incoming client-requests with incoming worker-connections")
        .arg(
            Arg::with_name("client-port")
                .long("client-port")
                .value_name("PORT")
                .help("Sets a custom client port (default 8080)")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("worker-port")
                .long("worker-port")
                .value_name("PORT")
                .help("Sets a custom worker port (default 8081)")
                .takes_value(true),
        )
        .get_matches();

    broker::serve(
        value_t!(matches.value_of("client-port"), u16).unwrap_or(8080),
        value_t!(matches.value_of("worker-port"), u16).unwrap_or(8081),
    )
    .await
}
