use futures::prelude::*;
use std::collections::VecDeque;
use std::sync::{Arc, Mutex};
use std::task::{Context, Poll, Waker};

#[derive(Debug)]
struct _Queue<T> {
    waiting_items: VecDeque<T>,
    waiting_consumers: VecDeque<Waker>,
}

impl<T> Default for _Queue<T> {
    fn default() -> Self {
        Self {
            waiting_items: Default::default(),
            waiting_consumers: Default::default(),
        }
    }
}

impl<T> _Queue<T>
where
    T: Send + 'static,
{
    fn provide(&mut self, item: T) {
        self.waiting_items.push_back(item);
        while let Some(waker) = self.waiting_consumers.pop_front() {
            waker.wake()
        }
    }

    fn poll_request(&mut self, ctx: &Context) -> Poll<T> {
        match self.waiting_items.pop_front() {
            Some(i) => Poll::Ready(i),
            None => {
                self.waiting_consumers.push_back(ctx.waker().clone());
                Poll::Pending
            }
        }
    }
}

#[derive(Debug)]
pub struct Queue<T> {
    inner: Arc<Mutex<_Queue<T>>>,
}

impl<T> Clone for Queue<T> {
    fn clone(&self) -> Self {
        Self {
            inner: self.inner.clone(),
        }
    }
}

impl<T> Queue<T>
where
    T: Send + 'static,
{
    pub fn new() -> Self {
        Self {
            inner: Arc::new(Mutex::new(Default::default())),
        }
    }

    pub fn provide(&self, item: T) {
        self.inner.lock().unwrap().provide(item)
    }

    pub fn request(&self) -> impl Future<Output = T> {
        let q = self.inner.clone();
        future::poll_fn(move |ctx| q.lock().unwrap().poll_request(ctx))
    }
}
