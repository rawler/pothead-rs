use futures::{
    channel::{mpsc, oneshot},
    future, FutureExt,
};

use hyper::server::{accept::Accept, conn::AddrIncoming, Server};
use hyper::service::{make_service_fn, service_fn};
use hyper::{client::conn::SendRequest, Body, Request, Response};
use std::pin::Pin;
use tokio::prelude::*;

use super::queueman::Queue;

#[derive(Debug)]
struct Error(Box<dyn std::fmt::Debug + 'static + Send + Sync>);

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

impl std::error::Error for Error {}

impl From<hyper::Error> for Error {
    fn from(e: hyper::Error) -> Self {
        Error(Box::new(e))
    }
}

impl From<mpsc::SendError> for Error {
    fn from(e: mpsc::SendError) -> Self {
        Error(Box::new(e))
    }
}

impl From<oneshot::Canceled> for Error {
    fn from(e: oneshot::Canceled) -> Self {
        Error(Box::new(e))
    }
}

async fn client_serve(
    worker_queue: Queue<SendRequest<Body>>,
    req: Request<Body>,
) -> Result<Response<Body>, Error> {
    let mut worker = loop {
        let mut worker = worker_queue.request().await;
        if future::poll_fn(|ctx| worker.poll_ready(ctx)).await.is_ok() {
            break worker;
        }
    };

    Ok(worker.send_request(req).await?)
}

async fn worker_serve(
    client: impl AsyncRead + AsyncWrite + Unpin + Send + 'static,
    worker_queue: Queue<SendRequest<Body>>,
) -> Result<(), Error> {
    let (send_request, c) = hyper::client::conn::handshake(client).await?;
    tokio::spawn(c.map(|_| ()));

    worker_queue.provide(send_request);
    Ok(())
}

async fn client_server_main(
    addr: std::net::SocketAddr,
    worker_queue: Queue<SendRequest<Body>>,
) -> Result<(), Error> {
    let handler = make_service_fn(move |_| {
        let queue = worker_queue.clone();
        future::ok::<_, Error>(service_fn(move |req| client_serve(queue.clone(), req)))
    });

    Server::bind(&addr)
        .serve(handler)
        .await
        .map_err(Error::from)
}

async fn worker_server_main(
    addr: std::net::SocketAddr,
    worker_queue: Queue<SendRequest<Body>>,
) -> Result<(), Error> {
    let mut addr_incoming = AddrIncoming::bind(&addr)?;
    while let Some(Ok(stream)) =
        future::poll_fn(|cx| Pin::new(&mut addr_incoming).poll_accept(cx)).await
    {
        let worker_queue = worker_queue.clone();
        tokio::spawn(worker_serve(stream.into_inner(), worker_queue).map(|_| ()));
    }
    Ok(())
}

pub async fn serve(client_port: u16, worker_port: u16) {
    // This is our socket address...
    let client_addr = ([0, 0, 0, 0], client_port).into();
    let worker_addr = ([0, 0, 0, 0], worker_port).into();

    let worker_queue = Queue::new();

    let client_result = tokio::spawn(client_server_main(client_addr, worker_queue.clone()));
    let worker_result = tokio::spawn(worker_server_main(worker_addr, worker_queue.clone()));

    future::select(client_result, worker_result).await;
}
